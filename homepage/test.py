from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from .views import index
from selenium import webdriver
import time
from selenium.webdriver.chrome.options import Options

class HomepageTestCase(TestCase):

    def test_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_index_use_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class FunctionalTestIndex(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')


    def tearDown(self):
        self.browser.quit()


    def test_title_index(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        self.assertIn('Profile', self.browser.title)

    def test_press_first_accordion_button_down_check_accordion_move_down(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        downButton = self.browser.find_element_by_name("down1")
        downButton.click()
        firstAccordion = self.browser.find_element_by_xpath("//div[@id='accordionExample']/div[@class='card'][2]")
        self.assertIn("accordion1", firstAccordion.get_attribute("id"))

    def test_press_second_accordion_button_up_check_accordion_move_up(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        downButton = self.browser.find_element_by_name("up2")
        downButton.click()
        firstAccordion = self.browser.find_element_by_xpath("//div[@id='accordionExample']/div[@class='card'][1]")
        self.assertIn("accordion2", firstAccordion.get_attribute("id"))